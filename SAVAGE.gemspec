Gem::Specification.new do |s|
  s.name        = 'SAVAGE'
  s.version     = '0.1.0'
  s.date        = '2013-06-17'
  s.summary     = "Text-editor friendly graphics language that compiles to SVG."
  s.description = "Savage is a graphics language layered on top of SVG, in the spirit of Sass and Haml."
  s.authors     = ["Mason Simon"]
  s.email       = "masonsimon@gmail.com"
  s.homepage    = "https://bitbucket.org/masonicboom/savage"

  s.files        = Dir["{lib}/**/*.rb", "bin/*", "LICENSE.txt", "*.md"]
  s.require_path = 'lib'
  s.executables  = ['savage']

  s.has_rdoc = false

  s.add_dependency 'sass'
end
