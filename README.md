# SAVAGE

## Intro

SAVAGE is a graphics language layered on top of SVG, in the spirit of Sass and Haml.

## Install

### From source

  1. clone repo
  1. bundle
  1. bundle exec bin/savage < test.svge > test.svg
  1. View test.svg in a web browser or other SVG renderer.

### From rubygems.org

  1. gem install SAVAGE
  1. rbenv rehash (if using rbenv)
  1. savage < your_test_file.svge > output.svg
  1. View output.svg in SVG renderer.

## Example

    // Set dimensions of your graphic (width x height).
    400 x 300

    // Draw a circle of radius 100 at (200,150).
    (100) @ 200,150

    // Draw an ellipse with x-radius 50 and y-radius 40 at 10,10.
    (50,40) @ 10,10

    // Draw a square of side length 10.
    [10] @ 100,200

    // Draw a rectangle of width 50, height 30.
    [50,30] @ 300,0

    // Draw a line from (20,30) to (40,50), then to (50,60).
    -
      @ 20,30
      @ 40,50
      @ 50,60

    // Draw an arrow.
    ~
      M 100,225
      L 100,115
      L 130,115
      L 70,15
      L 10,115
      L 40,115
      L 40,225
      z

    // Style using Sass.
    :sass
      .dot
        fill: red

      #point
        fill: blue

    [15].dot @ 300,250

    [15].dot @ 350,270

    (15)#point @ 360, 230

## License

Apache 2.0. See LICENSE.txt.

## Credits

Marc-André Cournoyer for writing [http://createyourproglang.com/](http://createyourproglang.com/), which got this project off the ground. The authors of [Haml](http://haml.info/), [Sass](http://sass-lang.com/), and [Coffeescript](http://coffeescript.org/), for demonstrating that we need not be content with the languages W3C gives us.

