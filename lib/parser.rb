require_relative 'parsers/svg'
require_relative 'parsers/sass'
require_relative 'parsers/circle'
require_relative 'parsers/ellipse'
require_relative 'parsers/round_rect'
require_relative 'parsers/rect'
require_relative 'parsers/text'
require_relative 'parsers/line'
require_relative 'parsers/path'

class Parser
  attr_reader :els

  def initialize(tokens)
    @els = Svg.new
    curr = @els

    # TODO: Switch to a state machine framework (state_machine, Ragel);
    # this is getting gross.
    tokens.each do |token|
      if curr == @els
        if token.first == :DIMENSION
          el = Svg.new
          el << token
          @els = el
          curr = el
        elsif token.first == :SASS
          el = SassEl.new
          el << token
          curr << el
          curr = el
        elsif token.first == :CIRCLE
          el = Circle.new
          el << token
          curr << el
          curr = el
        elsif token.first == :ELLIPSE
          el = Ellipse.new
          el << token
          curr << el
          curr = el
        elsif token.first == :ROUND_RECT
          el = RoundRect.new
          el << token
          curr << el
          curr = el
        elsif token.first == :RECT
          el = Rect.new
          el << token
          curr << el
          curr = el
        elsif token.first == :TEXT
          el = Text.new
          el << token
          curr << el
          curr = el
        elsif token.first == :LINE
          el = Line.new
          el << token
          curr << el
          curr = el
        elsif token.first == :PATH
          el = Path.new
          el << token
          curr << el
          curr = el
        end
      else
        if [:EOF, :DEDENT, :NEWLINE].include? token.first
          curr = @els
        else
          curr << token
        end
      end
    end
  end

  def translate
    @els.translate
  end
end

