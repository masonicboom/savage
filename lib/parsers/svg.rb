require_relative 'element'

class Svg < Element
  def initialize
    super
    @attributes = {
      'width' => 100,
      'height' => 100,
      'version' => '1.1',
      'xmlns' => 'http://www.w3.org/2000/svg',
    }
  end

  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :DIMENSION
      w, h = token.last
      @attributes['width'] = w
      @attributes['height'] = h
    end

    if token.is_a? Array
      if token.first == :ATTRIBUTE
        k, v = token.last
        @attributes[k] = v
      end
    elsif token.class < Element
      @children << token
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    """<svg #{attrs}>
  #{@children.map { |el| el.translate }.join("\n")}
</svg>"""
  end
end

