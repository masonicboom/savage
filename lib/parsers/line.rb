require_relative 'element'

class Line < Element
  def initialize
    super
    @points = []
  end

  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :LINE
    end

    if token.first == :LOCATION
      x, y = token.last
      @points << [x, y]
    elsif token.first == :ATTRIBUTE
      k, v = token.last
      @attributes[k] = v
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    points = @points.map { |coords| coords.join(',') }.join(' ')
    "<polyline points='#{points}' #{attrs} />"
  end
end

