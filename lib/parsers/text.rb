require_relative 'element'

class Text < Element
  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :TEXT
      @text = token.last
    end

    if token.first == :LOCATION
      x, y = token.last
      @attributes['x'] = x
      @attributes['y'] = y
    elsif token.first == :ATTRIBUTE
      k, v = token.last
      @attributes[k] = v
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    "<text #{attrs}>#{@text}</text>"
  end
end

