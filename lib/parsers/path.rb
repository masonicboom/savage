require_relative 'element'

class Path < Element
  def initialize
    super
    @pathdefs = []
  end

  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :PATH
    end

    if token.first == :LOCATION
      x, y = token.last
      @points << [x, y]
    elsif token.first == :ATTRIBUTE
      k, v = token.last
      @attributes[k] = v
    elsif token.first == :PATHDEF
      command, params = token.last
      @pathdefs << [command, params]
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    definition = @pathdefs.map { |command, params| "#{command} #{params}" }.join(' ')
    "<path d='#{definition}' #{attrs} />"
  end
end

