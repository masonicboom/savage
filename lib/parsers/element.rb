class Element
  def initialize
    @tokens = []
    @attributes = {}
    @children = []
  end

  def <<(token)
    if token.is_a? Array
      if token.first == :ID
        @attributes['id'] = token.last
      elsif token.first == :CLASS
        @attributes['class'] = token.last
      end
    end

    @tokens << token
  end
end

