require_relative 'element'

class Ellipse < Element
  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :ELLIPSE
      rx, ry = token.last
      @attributes['rx'] = rx
      @attributes['ry'] = ry
    end

    if token.first == :LOCATION
      x, y = token.last
      @attributes['cx'] = x
      @attributes['cy'] = y
    elsif token.first == :ATTRIBUTE
      k, v = token.last
      @attributes[k] = v
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    "<ellipse #{attrs} />"
  end
end

