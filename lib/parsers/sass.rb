require_relative 'element'
require 'sass'

class SassEl < Element
  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :SASS

      # Dedent the text.
      lines = token.last.split("\n")
      indent = lines.first.match(/\A(\s+)/)[1].size
      @sass = lines.map { |l| l[indent..-1] }.join("\n")
    end

    super(token)
  end

  def translate
    css = Sass::Engine.new(@sass).render
    """<style type='text/css' media='screen'>
  <![CDATA[
    #{css}
  ]]>
</style>"""
  end
end

