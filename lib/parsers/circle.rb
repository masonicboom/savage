require_relative 'element'

class Circle < Element
  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :CIRCLE
      radius = token.last
      @attributes['r'] = radius
    end

    if token.first == :LOCATION
      x, y = token.last
      @attributes['cx'] = x
      @attributes['cy'] = y
    elsif token.first == :ATTRIBUTE
      k, v = token.last
      @attributes[k] = v
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    "<circle #{attrs} />"
  end
end

