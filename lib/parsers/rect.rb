require_relative 'element'

class Rect < Element
  def <<(token)
    if @tokens.empty?
      raise "Invalid starting token: #{token}" unless token.first == :RECT
      w, h = token.last
      @attributes['width'] = w
      @attributes['height'] = h
    end

    if token.first == :LOCATION
      x, y = token.last
      @attributes['x'] = x
      @attributes['y'] = y
    elsif token.first == :ATTRIBUTE
      k, v = token.last
      @attributes[k] = v
    end

    super(token)
  end

  def translate
    attrs = @attributes.map { |k,v| "#{k}='#{v}'" }.join(' ')
    "<rect #{attrs} />"
  end
end

