class Lexer
  def tokenize(code)
    code.chomp!

    tokens = []

    # TODO: Switch to a state machine framework (state_machine, Ragel);
    # this is getting gross.

    # Advance 1 char at a time until finding something parsable.
    i = 0

    indented = false

    while i < code.size
      chunk = code[i..-1]

      if comment = chunk.match(/\A\/\/.*?\n/)
        i += comment.to_s.size - 1
      elsif dimension = chunk.match(/\A(\d+)\sx\s(\d+)/)
        w, h = dimension.captures.map(&:to_i)
        tokens << [:DIMENSION, [w, h]]
        i += dimension.to_s.size
      elsif sass = chunk.match(/\A\:sass\n((?:(?:[ ]+.*?\n)|\n)+)/)
        src = sass[1]
        indented = true
        tokens << [:SASS, src]
        i += sass.to_s.size - 1
      elsif circle = chunk.match(/\A\((\d+)\)/)
        r = circle[1].to_i
        tokens << [:CIRCLE, r]
        i += circle.to_s.size
      elsif ellipse = chunk.match(/\A\((\d+),\s*(\d+)\)/)
        rx, ry = ellipse.captures.map(&:to_i)
        tokens << [:ELLIPSE, [rx, ry]]
        i += ellipse.to_s.size
      elsif round_rect = chunk.match(/\A\[(\d+),\s*(\d+),\s*(\d+),\s*(\d+)\)/)
        w, h, rx, ry = round_rect.captures.map(&:to_i)
        tokens << [:ROUND_RECT, [w, h, rx, ry]]
        i += round_rect.to_s.size
      elsif rect = chunk.match(/\A\[(\d+),\s*(\d+)\]/)
        w, h = rect.captures.map(&:to_i)
        tokens << [:RECT, [w, h]]
        i += rect.to_s.size
      elsif square = chunk.match(/\A\[(\d+)\]/)
        s = square[1].to_i
        tokens << [:RECT, [s, s]]
        i += square.to_s.size
      elsif text = chunk.match(/\A\"([^\"]+)\"/)
        t = text[1]
        tokens << [:TEXT, t]
        i += text.to_s.size
      elsif line = chunk.match(/\A\-/)
        tokens << [:LINE]
        i += line.to_s.size
      elsif path = chunk.match(/\A\~/)
        tokens << [:PATH]
        i += path.to_s.size
      elsif pathdef = chunk.match(/\A([MmZzLlHhVvCcSsQqTtAa])\s?((?:[\d\.\-,](?: )?)+)?/)
        command, params = pathdef.captures
        tokens << [:PATHDEF, [command, params]]
        i += pathdef.to_s.size
      elsif id = chunk.match(/\A#([a-zA-Z0-9_\-]+)/)
        name = id[1]
        tokens << [:ID, name]
        i += id.to_s.size
      elsif classname = chunk.match(/\A\.([a-zA-Z0-9_\-]+)/)
        name = classname[1]
        tokens << [:CLASS, name]
        i += classname.to_s.size
      elsif coords = chunk.match(/\A@\s*(\d+)\s*,\s*(\d+)/)
        x, y = coords.captures
        tokens << [:LOCATION, [x.to_i, y.to_i]]
        i += coords.to_s.size
      elsif chunk.match(/\A\n\n/)
        tokens << [:NEWLINE]
        i += 1
      elsif indent = chunk.match(/\A\n\s+/)
        indented = true
        tokens << [:INDENT]
        i += indent.to_s.size
      elsif indented && chunk.match(/\A\n[^\s]/)
        indented = false
        tokens << [:DEDENT]
        i += 1
      elsif indented && (attribute = chunk.match(/\A(\w+):\s*([^\s]+?)(?:\n|\Z)/))
        key, value = attribute.captures
        tokens << [:ATTRIBUTE, [key, value]]
        i += attribute.to_s.size - 1 # Don't advance beyond newline.
      else
        i += 1
      end
    end
    tokens << [:EOF]

    tokens
  end
end


